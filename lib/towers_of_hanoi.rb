# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_accessor :towers
  def initialize
    @towers = [[3,2,1],[],[]]
  end

  def move(from_tower, to_tower)
    @towers[to_tower] << @towers[from_tower].pop
  end

  def valid_move?(from_tower, to_tower)
    valid_answers = [0,1,2]
    if valid_answers.include?(from_tower) == false || valid_answers.include?(to_tower) == false
      return false
    end

    if @towers[from_tower].empty? == false
      if @towers[to_tower].empty?
        return true
      elsif @towers[from_tower].last < @towers[to_tower].last
        return true
      else
        return false
      end
    else
      return false
    end

  end

  def render
    top_row = @towers.map {|tower| tower.size >= 3 ? tower[2] : " " }
    mid_row = @towers.map {|tower| tower.size >= 2 ? tower[1] : " " }
    bot_row = @towers.map {|tower| tower.size >= 1 ? tower[0] : " " }
    "#{top_row.join(" ")}\n#{mid_row.join(" ")}\n#{bot_row.join(" ")}\n_ _ _"
  end

  def won?
    return true if @towers[1].size == 3 || @towers[2].size == 3
    return false
  end

  def play
    won = false
    puts render
    while won == false
      print "Which tower would you like to move a disk from? Enter 1, 2 or 3: "
      from_tower = gets.to_i-1
      print "Which tower would you like to move a disk to? Enter 1, 2 or 3: "
      to_tower = gets.to_i-1
      if self.valid_move?(from_tower,to_tower)
        self.move(from_tower,to_tower)
        puts render
      else
        puts "Not a valid move"
      end
      if self.won?
        won = true
      end
    end
    print "You win!"
  end
end
